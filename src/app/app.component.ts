import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})

export class AppComponent implements OnInit {

  subjects;
  constructor(private router:Router) {}

  ngOnInit() {
    this.subjects = [
      {
        name: 'All'
      },
      {
        img: '',
        name: 'Avisos'
      },
      {
        img: '',
        name: 'mxn'
      }
    ];
  }

  goToSubject(){

  }

}
