import { Component, OnInit } from '@angular/core';
import { LoadingController, ModalController } from '@ionic/angular';
import { AuthService} from './../services/api/auth.service';
import { Avisos } from '../models/lista.interface';
import { ModalPage } from '../pages/modal/modal.page';
import { Modal2Page } from '../pages/modal2/modal2.page';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage implements OnInit {

  
  avisos:Avisos[];
  constructor(private api:AuthService, private modalCtrl: ModalController, private modalCtrl2: ModalController) {}

  ngOnInit():void{
    this.api.getAll(1).subscribe(data =>{
      console.log(data);
      this.avisos = data;
    })
  }

  async abrirModal(){
    const modal = await this.modalCtrl.create({
      component: ModalPage,
      componentProps:{

      }
    });
    await modal.present();
  }

  async abrirModal2(id){
    //console.log(id);
    const modal2 = await this.modalCtrl2.create({
      component: Modal2Page,
      componentProps:{
        id : id
      }
    });
    await modal2.present();
  }



}
