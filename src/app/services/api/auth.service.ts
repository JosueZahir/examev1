import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AvisosPost } from 'src/app/models/avisos.interface';
import { Avisos } from 'src/app/models/lista.interface';
import { ResponseI } from '../../models/response.interface';

@Injectable({
    providedIn: 'root'
})

export class AuthService{

    url : string = "http://54.69.118.16:8090/api/";

    constructor(private http:HttpClient){

    }

    
    //api
    getAll(page:number):Observable<Avisos[]>{
    let direccion = this.url + "avisos";
    return this.http.get<Avisos[]>(direccion); 
    }

    postAviso(form:AvisosPost): Observable<ResponseI>{
        let direccion = this.url + 'nuevoAviso';
        return this.http.post<ResponseI>(direccion, form);
    }

    putAvisos(form:Avisos):Observable<ResponseI>{
        let direccion = this.url + 'actualizaAvisos';
        return this.http.put<ResponseI>(direccion, form);
    }
}