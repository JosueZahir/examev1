import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import { AuthService } from '../../services/api/auth.service';
import { AlertController} from '@ionic/angular';
import { AvisosPost } from '../../models/avisos.interface';
import { ResponseI} from '../../models/response.interface';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { dismiss } from '@ionic/core/dist/types/utils/overlays';
import { ModalController} from '@ionic/angular';


@Component({
  selector: 'app-modal',
  templateUrl: './modal.page.html',
  styleUrls: ['./modal.page.scss'],
})
export class ModalPage implements OnInit {
  @Input() formControl: "nuevoAviso";
  
  
  nuevoAviso = new FormGroup({
    aviso: new FormControl ('', [Validators.required, Validators.minLength(5)]),
    fecha: new FormControl (''),
    imagen: new FormControl ('', [Validators.required, Validators.minLength(4)]),
    tipoUsuario: new FormControl ('',[Validators.required, Validators.minLength(4)]),
    titulo: new FormControl ('',[Validators.required, Validators.minLength(4)]),
  });
  result:string;
  constructor(private api: AuthService, public alertC: AlertController, public httpClient: HttpClient, private modalCtrl: ModalController) { }

  async alertS() {
    const alert = await this.alertC.create({
      cssClass: 'my-custom-class',
      header: 'Registro Exitoso',
      message: 'El registro fue exitoso',
      buttons: ['OK']
    });
    await alert.present();
  }

  async alertError() {
    const alert = await this.alertC.create({
      cssClass: 'my-custom-class',
      header: 'Error',
      message: 'Los datos no se agregaron',
      buttons: ['OK']
    });
    await alert.present();

  }

  ngOnInit() {
    let token=localStorage.getItem('token');
    this.nuevoAviso.patchValue({
      'token':token
    });
  }
  /*
  sendPost(){
    this.nuevoAviso.controls.aviso.setValue();
  }*/
  guardarDatos(){
    
    /*let postData = {
      "aviso": "prueba",
      "fecha":"2023-06-28T05:00:00.000+00:00",
      "imagen": "prueba",
      "tipoUsuario": "prueba",
      "titulo": "prueba"
    }*/

     let postData = {
      "aviso": "prueba",
      "fecha":"2023-06-28T05:00:00.000+00:00",
      "imagen": "prueba",
      "tipoUsuario": "prueba",
      "titulo": "prueba"
    }
    //console.log(this.aviso.value);
    this.httpClient.post("http://54.69.118.16:8090/api/nuevoAviso", this.nuevoAviso.value).subscribe(data =>{
      console.log(this.nuevoAviso.value);
      this.alertS();
      
    }, error =>{
      console.log(error);
    });
  }

  postForm(form:AvisosPost){
    this.api.postAviso(form).subscribe(data =>{
      let respuesta: ResponseI = data;
      
      if(respuesta.status == "ok"){
        //this.alertS();
        console.log("Datos ingresados");
      }else{
        console.log("datos no ingresados");
        this.alertError();
      }

    });
  }
}
