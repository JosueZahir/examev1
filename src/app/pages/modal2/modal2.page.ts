import { Component, OnInit, Input } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormControl, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import { Avisos } from '../../models/lista.interface';
import { AuthService } from '../../services/api/auth.service';
import { AlertController} from '@ionic/angular';
import { AvisosPost } from '../../models/avisos.interface';
import { ResponseI} from '../../models/response.interface';
import { dismiss } from '@ionic/core/dist/types/utils/overlays';
import { ModalController} from '@ionic/angular';

@Component({
  selector: 'app-modal2',
  templateUrl: './modal2.page.html',
  styleUrls: ['./modal2.page.scss'],
})
export class Modal2Page implements OnInit {

  @Input("id") id; 
  
  datosAvisos: Avisos;

  actualizarAviso = new FormGroup({
    id: new FormControl (''),
    aviso: new FormControl ('', [Validators.required, Validators.minLength(5)]),
    tipoUsuario: new FormControl ('',[Validators.required, Validators.minLength(4)]),
    fecha: new FormControl (''),
    imagen: new FormControl ('', [Validators.required, Validators.minLength(4)]),
    titulo: new FormControl ('',[Validators.required, Validators.minLength(4)]),
    creado: new FormControl ('', [Validators.required]),
    update: new FormControl ('', [Validators.required])
  });

  constructor(public httpClient: HttpClient) { }

  ngOnInit() :void {
    //console.log("http://54.69.118.16:8090/api/aviso/" + this.id);
    
    this.httpClient.get("http://54.69.118.16:8090/api/aviso/" + this.id).subscribe(data =>{

      this.actualizarAviso.setValue({
        id: [this.datosAvisos.id],
        aviso: [this.datosAvisos.aviso],
        tipoUsuario: [this.datosAvisos.tipoUsuario],
        fecha: [this.datosAvisos.fecha],
        imagen: [this.datosAvisos.imagen],
        titulo: [this.datosAvisos.titulo],
        creado: [this.datosAvisos.creado],
        update: [this.datosAvisos.update]
      });
      
      console.log(data);
      }, error =>{
        console.log(error);
      
    });
    

  }

}
