export interface Avisos{
        id: number
        aviso: string
        tipoUsuario: string
        fecha: string
        imagen: string
        titulo: string 
        creado: string
        update: string
}